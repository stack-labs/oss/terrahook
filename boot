#!/bin/sh

red='\033[0;31m'
green='\033[0;32m'
orange='\033[0;33m'
nocolor='\033[0m'
params=""
hooks=""

help() {
  cat <<-EOT
  Description:
    Terrahook is a bundled combination of terraform and
    github.com/adnanh/webhook to have an HTTP(S) callable platform manager

  Options:
    HELP                 < $(printf ${green}${HELP:-$(printf "${orange}undefined")}${nocolor}) >
                         Set this environment variable to show this help message
    GOOGLE_CREDENTIALS   < $(printf ${green}${GOOGLE_CREDENTIALS:-$(printf "${orange}undefined")}${nocolor}) >
                         Set this environment variable with either a path to valid JSON service
                         account credentials or a directly as a JSON string
    PORT                 < $(printf ${green}${PORT:-8080}${nocolor}) >
                         Set this environment variable to listen to another port than default 8080
    VERBOSE              < $(printf ${green}${VERBOSE:-$(printf "${orange}undefined")}${nocolor}) >
                         Set this environment variable to turn verbose mode
    PREFIX               < $(printf ${green}${PREFIX:-$(printf "${orange}undefined")}${nocolor}) >
                         Set this environment variable with an URL prefix to add it to every hook
                         defined in the configuration
    HOOKS_CONFIGURATION  < $(printf ${green}${HOOKS_CONFIGURATION:-./webhook.json}${nocolor}) >
                         Set this environment variable to point to a specific webhook configuration file

EOT
  exit 0
}

authenticate() {
  set -e

  if [ -n "${GOOGLE_CREDENTIALS}" ]; then
    export GOOGLE_APPLICATION_CREDENTIALS
    if [ -e "${GOOGLE_CREDENTIALS}" ]; then
      GOOGLE_APPLICATION_CREDENTIALS="${GOOGLE_KEY}"
    else
      GOOGLE_APPLICATION_CREDENTIALS=$(mktemp)
      echo "${GOOGLE_CREDENTIALS}" > "${GOOGLE_APPLICATION_CREDENTIALS}"
    fi
    gcloud auth activate-service-account --key-file "${GOOGLE_APPLICATION_CREDENTIALS}"
  fi
}

parse_options() {
  set -e

  if [ -n "${VERBOSE}" ];             then params="${params} -verbose";                                                    fi
  if [ -n "${PREFIX}" ];              then params="${params} -urlprefix ${PREFIX}";  else params="${params} -urlprefix=";  fi
  if [ -n "${PORT}" ];                then params="${params} -port ${PORT}";         else params="${params} -port 8080";   fi
  if [ -n "${HOOKS_CONFIGURATION}" ]; then hooks="${HOOKS_CONFIGURATION}";           else hooks="./webhook.json";          fi
}

validate_webhook() {
  set -e

  run_script=$(mktemp)
  cp "./run" "${run_script}"
  chmod +x "${run_script}"

  hooks_configuration=$(mktemp)
  jq -e \
    "map(.[\"execute-command\"] = \"${run_script}\")" \
    "${hooks}" 1> "${hooks_configuration}" \
    || return $?

  params="${params} -hooks ${hooks_configuration}"
}

procedure() {
  function=$1
  message=$2
  if ${function}; then
    printf "[${green}OK${nocolor}] ${message}";
    printf "\n"
  else
    err=$?
    printf "[${red}KO${nocolor}] ${message}"
    printf "\n"
    exit ${err}
  fi
}

boot() {
  set -e
  procedure "parse_options"       "Optional arguments parsing"
  procedure "validate_webhook"    "Validating webhook configuration"
  procedure "authenticate"        "Authenticating"

  webhook ${params}
}

if [ -n "${HELP}" ]; then help; else boot; fi
