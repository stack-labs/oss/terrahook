#!/bin/sh

action=""
payload=""
working_directory=""

help() {
  cat <<-EOT
  Usage: run COMMAND PAYLOAD TEMPLATES [-h|--help]

  Options and arguments:
    -h, --help      Show this message
EOT
  exit 0
}

while [ $# -gt 0 ]
do
  case "$1" in
    -h|--help)
      help
      ;;
    apply|destroy)
      action="$1"
      shift
      ;;
    *)
    payload=$1
    shift
    ;;
  esac
done

if [ -z "${action}" ];  then help;  fi
if [ -z "${payload}" ]; then help;  fi

templates() {
  jq -r -n --argjson data "${payload}" '$data.templates'
}

backend_config() {
  set -e
  repository=$(jq -r -n --argjson data "${payload}" '$data.repository' || return $?)
  IFS='/' read -r bucket prefix <<-_EOF_
$repository
_EOF_
  config="-backend-config bucket=${bucket}"
  if [ -n "${prefix}" ]; then
    config="${config} -backend-config prefix=${prefix}"
  fi
  echo "${config}"
}

load_terraform() {
  set -e
  working_directory=$(mktemp -d)
  gsutil -m cp -r "gs://$(templates)"/* "${working_directory}"
}

validate_terraform() {
  set -e
  tflint "${working_directory}"
}

terraform_payload() {
  jq -r -n --argjson data "${payload}" '$data.variables | to_entries | map("-var '"\(.key)=\(.value)"'") | join(" ")'
}

apply() {
  set -e
  load_terraform
  validate_terraform
  cd "${working_directory}" && \
    terraform init $(backend_config) && \
    terraform apply $(terraform_payload) -auto-approve
}

destroy() {
  set -e
  load_terraform
  validate_terraform
  cd "${working_directory}" && \
    terraform init $(backend_config) && \
    terraform destroy $(terraform_payload) -auto-approve
}

${action}
