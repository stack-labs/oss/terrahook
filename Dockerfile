FROM        golang:alpine AS WEBHOOK
RUN         apk add --update git && \
            go get github.com/adnanh/webhook
WORKDIR     /go/src/github.com/adnanh/webhook
RUN         CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o webhook github.com/adnanh/webhook && \
            chmod +x ./webhook

FROM        golang:alpine AS TERRAFORM
RUN         apk add --update git bash openssh
ENV         TERRAFORM_VERSION=0.12.26
ENV         TF_DEV=true
ENV         TF_RELEASE=true
WORKDIR     /go/src/github.com/hashicorp/terraform
RUN         git clone https://github.com/hashicorp/terraform.git ./ && \
            git checkout v${TERRAFORM_VERSION} && \
            CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o terraform github.com/hashicorp/terraform && \
            chmod +x ./terraform

FROM        golang:1.14.3-alpine3.11 as TFLINT
RUN         apk --no-cache add git make gcc musl-dev zip
ENV         TFLINT_VERSION=0.17.0
WORKDIR     /go/src/github.com/terraform-linters/tflint
RUN         git clone https://github.com/terraform-linters/tflint ./ && \
            git checkout v${TFLINT_VERSION} && \
            make build && \
            chmod +x ./dist/tflint

FROM        google/cloud-sdk:alpine
RUN         apk --no-cache add ca-certificates && \
            apk update && \
            apk add jq && \
            rm -rf /var/cache/apk/*
COPY        --from=WEBHOOK /go/src/github.com/adnanh/webhook/webhook /bin
COPY        --from=TERRAFORM /go/src/github.com/hashicorp/terraform/terraform /bin
COPY        --from=TFLINT /go/src/github.com/terraform-linters/tflint/dist/tflint /bin

WORKDIR     /terrahook/
COPY        boot            .
COPY        webhook.json    .
COPY        run             .

ENTRYPOINT  ["./boot"]

